﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using MyLibraryManagment.dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLibraryManagment.dal
{
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options) : base(options)
        {
               
        }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Book> Books { get; set; }

        protected override  void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Author>().HasData(
                 new Author
                 {
                     AuthorId = 1,
                     Name = "William",

                 },
                  new Author
                  {
                      AuthorId = 2,
                      Name = "James",

                  }
             );

            modelBuilder.Entity<Customer>().HasData(
                new Customer
                {
                    CustomerId = 1,
                    name = "Street",

                },
                 new Customer
                 {
                     CustomerId = 2,
                     name = "Fighter",

                 }
            );
            modelBuilder.Entity<Book>().HasData(
               new Book { BookId = 4, AuthorId = 1, title = "New life" },
               new Book { BookId = 5, AuthorId = 1, title = "Enjoy life", CustomerId = 1 },
               new Book { BookId = 6, AuthorId = 1, title = "Hopeful life", CustomerId = 1 }
           );
            modelBuilder.Entity<Book>().HasData(
                new Book { BookId = 1, AuthorId = 2, title = "Hamlet", CustomerId = 1 },
                new Book { BookId = 2, AuthorId = 2, title = "King Lear", CustomerId = 2 },
                new Book { BookId = 3, AuthorId = 2, title = "Othello", CustomerId = 2 }
            );


        }
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //  optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["BloggingDatabase"].ConnectionString);
        //}

    }
}
