﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyLibraryManagment.dal.models
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        public string name { get; set; }
        public ICollection<Book> books { get; set; }

    }
}
