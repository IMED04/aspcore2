﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyLibraryManagment.dal.models
{
    public class Book
    {
        [Key]
        public int BookId { get; set; }
        public string  title { get; set; }
        
        public int AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        public virtual Author  Author {get;set;}
        
        public int? CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }


    }
}