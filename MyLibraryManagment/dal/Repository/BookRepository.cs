﻿using Microsoft.EntityFrameworkCore;
using MyLibraryManagment.dal.Interfaces;
using MyLibraryManagment.dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLibraryManagment.dal.Repository
{
    public class BookRepository : Repository<Book>, IBookRepository
    {

        public BookRepository(LibraryDbContext context):base(context)
        {

        }
        public IEnumerable<Book> FindWithAuthor(Func<Book, bool> predicate)
        {

            return _context.Books.Include(a => a.Author).Where(predicate);
           
        }

        public IEnumerable<Book> FindWithAuthorAndBorrower(Func<Book, bool> predicate)
        {
            return _context.Books.Include(a => a.Author).Include(a=>a.Customer).Where(predicate);
        }

        public IEnumerable<Book> GetAllWithAuthorAndBorrower()
        {
            return _context.Books.Include(a =>a.Author).Include(b=>b.Customer).ToList();
        }

    }
}
