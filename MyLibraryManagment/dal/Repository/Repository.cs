﻿using Microsoft.EntityFrameworkCore;
using MyLibraryManagment.dal.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLibraryManagment.dal.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
       protected LibraryDbContext _context;
        public Repository(LibraryDbContext context)
        {
            _context = context;
        }
        protected void save() => _context.SaveChanges();
        public int Count(Func<T, bool> predicate)
        {
            return _context.Set<T>().Where(predicate).Count();
        }

        public void Create(T entity)
        {
            _context.Add(entity);
            save();
        }

        public void Delete(T entity)
        {
            _context.Remove(entity);
            save();
        }

        public IEnumerable<T> find(Func<T, bool> preducate)
        {
            return _context.Set<T>().Where(preducate);
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            save();
        }
    }
}
