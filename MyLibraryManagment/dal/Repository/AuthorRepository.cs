﻿using Microsoft.EntityFrameworkCore;
using MyLibraryManagment.dal.Interfaces;
using MyLibraryManagment.dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLibraryManagment.dal.Repository
{
    public class AuthorRepository : Repository<Author>, IAuthorRepository
    {

        public AuthorRepository(LibraryDbContext context):base(context)
        {

        }
        public IEnumerable<Author> GetAllWithBooks()
        {
            return _context.Authors.Include(a => a.books);

        }


        public Author GetWithBooks(int id)
        {
            return _context.Authors.Where(a => a.AuthorId == id).Include(a => a.books).FirstOrDefault(); ;
        }
    }
}
