﻿using MyLibraryManagment.dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLibraryManagment.dal.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
