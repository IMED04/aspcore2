﻿using MyLibraryManagment.dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLibraryManagment.dal.Interfaces
{
    public interface IBookRepository :IRepository<Book>
    {
        IEnumerable<Book> GetAllWithAuthorAndBorrower();
        IEnumerable<Book> FindWithAuthor(Func<Book, bool> predicate);
        IEnumerable<Book> FindWithAuthorAndBorrower(Func<Book, bool> predicate);

    }
}
