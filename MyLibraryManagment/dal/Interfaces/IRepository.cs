﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLibraryManagment.dal.Interfaces
{
  public  interface IRepository<T> where T  : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> find(Func<T, bool> preducate);
        T GetById(int id);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        int Count(Func<T, bool> predicate);
    }
}
