﻿using Microsoft.AspNetCore.Mvc;
using MyLibraryManagment.dal.Interfaces;
using MyLibraryManagment.dal.models;
using MyLibraryManagment.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//Modele maison
namespace MyLibraryManagment.Controllers
{
    public class CustomerController:Controller
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IBookRepository _bookRepository;

        public CustomerController(ICustomerRepository customerRepository ,IBookRepository bookRepository)
        {
            _customerRepository = customerRepository;
            _bookRepository = bookRepository;
        }
        [Route("customer" ,Name ="list")]
        public IActionResult List()
        {
            var customerVM = new List<CustomerViewModel>();
            var customers = _customerRepository.GetAll();

            if (customers.Count() == 0)
            {
                return View("Empty");
            }
            foreach (var customer in customers)
            {
                customerVM.Add(new CustomerViewModel
                {
                    Customer = customer,
                BookCount=_bookRepository.Count(x=>x.CustomerId== customer.CustomerId)
                });

            }

            return View(customerVM);
        }

        public IActionResult Edit(int id)
        {
            var customer = _customerRepository.GetById(id);
            return View(customer);
        }
        [HttpPost]
        public IActionResult Edit(Customer customer)
        {
          
            _customerRepository.Update(customer);
            return RedirectToRoute("List");
        }
        public IActionResult Delete(int id)
        {
            var customer = _customerRepository.GetById(id);
            _customerRepository.Delete(customer);
            return RedirectToAction("List");
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Customer customer)
        {

            _customerRepository.Create(customer);
            return RedirectToRoute("list");
        }

    }
}
